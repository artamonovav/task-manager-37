package ru.t1.artamonov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.artamonov.tm.api.model.ICommand;
import ru.t1.artamonov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @NotNull
    private static final String NAME = "help";

    @NotNull
    private static final String ARGUMENT = "-h";

    @NotNull
    private static final String DESCRIPTION = "Display list of terminal command.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (@NotNull final ICommand command : commands) System.out.println(command);
    }

}
