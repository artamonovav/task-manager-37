package ru.t1.artamonov.tm.repository;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.constant.DmlConstant;
import ru.t1.artamonov.tm.api.repository.IProjectRepository;
import ru.t1.artamonov.tm.enumerated.Status;
import ru.t1.artamonov.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Getter
    @NotNull
    private final String tableName = DmlConstant.PROJECT_TABLE;

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Project fetch(@NotNull final ResultSet row) {
        @NotNull final Project project = new Project();
        project.setId(row.getString(DmlConstant.ROW_ID));
        project.setName(row.getString(DmlConstant.PROJECT_NAME));
        project.setDescription(row.getString(DmlConstant.PROJECT_DESCRPTN));
        project.setUserId(row.getString(DmlConstant.USER_ID));
        project.setStatus(Status.toStatus(row.getString(DmlConstant.PROJECT_STATUS)));
        project.setCreated(row.getTimestamp(DmlConstant.PROJECT_CREATED));
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project add(@NotNull final Project project) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?);",
                getTableName(),
                DmlConstant.ROW_ID,
                DmlConstant.PROJECT_CREATED,
                DmlConstant.PROJECT_NAME,
                DmlConstant.PROJECT_DESCRPTN,
                DmlConstant.PROJECT_STATUS,
                DmlConstant.USER_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getId());
            statement.setTimestamp(2, new Timestamp(project.getCreated().getTime()));
            statement.setString(3, project.getName());
            statement.setString(4, project.getDescription());
            statement.setString(5, Status.NOT_STARTED.name());
            statement.setString(6, project.getUserId());
            statement.executeUpdate();
        }
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project add(@Nullable final String userId, @Nullable final Project project) {
        project.setUserId(userId);
        return add(project);
    }

    @Override
    @SneakyThrows
    public Project update(@NotNull final Project project) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ? WHERE %s = ?;",
                getTableName(),
                DmlConstant.PROJECT_NAME, DmlConstant.PROJECT_DESCRPTN,
                DmlConstant.STATUS, DmlConstant.ROW_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getName());
            statement.setString(2, project.getDescription());
            statement.setString(3, project.getStatus().toString());
            statement.setString(4, project.getId());
            statement.executeUpdate();
        }
        return project;
    }

}
