package ru.t1.artamonov.tm.api.service;

import ru.t1.artamonov.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {

}
