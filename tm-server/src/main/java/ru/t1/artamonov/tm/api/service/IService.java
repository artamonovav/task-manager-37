package ru.t1.artamonov.tm.api.service;

import ru.t1.artamonov.tm.api.repository.IRepository;
import ru.t1.artamonov.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}
