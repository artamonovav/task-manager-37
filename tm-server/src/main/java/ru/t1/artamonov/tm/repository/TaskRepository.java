package ru.t1.artamonov.tm.repository;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.constant.DmlConstant;
import ru.t1.artamonov.tm.api.repository.ITaskRepository;
import ru.t1.artamonov.tm.enumerated.Status;
import ru.t1.artamonov.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Getter
    @NotNull
    private final String tableName = DmlConstant.TASK_TABLE;

    public TaskRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Task fetch(@NotNull final ResultSet row) {
        @NotNull final Task task = new Task();
        task.setId(row.getString(DmlConstant.ROW_ID));
        task.setName(row.getString(DmlConstant.TASK_NAME));
        task.setDescription(row.getString(DmlConstant.TASK_DESCRPTN));
        task.setUserId(row.getString(DmlConstant.USER_ID));
        task.setStatus(Status.toStatus(row.getString(DmlConstant.TASK_STATUS)));
        task.setCreated(row.getTimestamp(DmlConstant.TASK_CREATED));
        task.setProjectId(row.getString(DmlConstant.TASK_PROJECT_ID));
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(@NotNull final Task task) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?, ?);",
                getTableName(),
                DmlConstant.ROW_ID, DmlConstant.TASK_CREATED, DmlConstant.TASK_NAME,
                DmlConstant.TASK_DESCRPTN, DmlConstant.TASK_STATUS, DmlConstant.USER_ID,
                DmlConstant.TASK_PROJECT_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getId());
            statement.setTimestamp(2, new Timestamp(task.getCreated().getTime()));
            statement.setString(3, task.getName());
            statement.setString(4, task.getDescription());
            statement.setString(5, Status.NOT_STARTED.name());
            statement.setString(6, task.getUserId());
            statement.setString(7, task.getProjectId());
            statement.executeUpdate();
        }
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task add(@Nullable final String userId, @Nullable final Task task) {
        task.setUserId(userId);
        System.out.println(task.getUserId());
        return add(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? AND %s = ?;",
                getTableName(), DmlConstant.USER_ID, DmlConstant.TASK_PROJECT_ID

        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @Override
    @SneakyThrows
    public Task update(@NotNull final Task task) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?;",
                getTableName(), DmlConstant.TASK_NAME, DmlConstant.TASK_DESCRPTN,
                DmlConstant.TASK_STATUS, DmlConstant.TASK_PROJECT_ID, DmlConstant.ROW_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getName());
            statement.setString(2, task.getDescription());
            statement.setString(3, task.getStatus().toString());
            statement.setString(4, task.getProjectId());
            statement.setString(5, task.getId());
            statement.executeUpdate();
        }
        return task;
    }

    @Override
    @SneakyThrows
    public void removeTasksByProjectId(@NotNull final String projectId) {
        @NotNull final String query = String.format("DELETE FROM %s WHERE %s = ?;",
                getTableName(), DmlConstant.TASK_PROJECT_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, projectId);
            statement.executeUpdate();
        }
    }

}
