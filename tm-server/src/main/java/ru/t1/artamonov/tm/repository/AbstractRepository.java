package ru.t1.artamonov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.constant.DmlConstant;
import ru.t1.artamonov.tm.api.repository.IRepository;
import ru.t1.artamonov.tm.comparator.CreatedComparator;
import ru.t1.artamonov.tm.comparator.StatusComparator;
import ru.t1.artamonov.tm.model.AbstractModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @NotNull
    protected abstract String getTableName();

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (CreatedComparator.INSTANCE.equals(comparator)) return DmlConstant.CREATED;
        if (StatusComparator.INSTANCE.equals(comparator)) return DmlConstant.STATUS;
        return DmlConstant.NAME;
    }

    @NotNull
    protected abstract M fetch(@NotNull final ResultSet row);

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String sql = String.format("DELETE FROM %s;", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()
        ) {
            statement.execute(sql);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s;", getTableName());
        try (@NotNull final Statement statement = connection.createStatement();
             @NotNull final ResultSet resultSet = statement.executeQuery(sql)
        ) {
            while (resultSet.next()) result.add(fetch(resultSet));
            return result;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s ORDER BY %s;", getTableName(), getSortType(comparator));
        try (@NotNull final Statement statement = connection.createStatement();
             @NotNull final ResultSet resultSet = statement.executeQuery(sql)
        ) {
            while (resultSet.next()) result.add(fetch(resultSet));
            return result;
        }
    }

    @NotNull
    @Override
    public abstract M add(@NotNull final M model);

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        List<M> result = new ArrayList<>();
        for (M model : models) {
            result.add(add(model));
        }
        return result;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@NotNull final String id) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? LIMIT 1;", getTableName(), DmlConstant.ROW_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            statement.setString(1, id);
            try (@NotNull final ResultSet rowSet = statement.executeQuery()) {
                if (!rowSet.next()) return null;
                return fetch(rowSet);
            }
        }
    }

    @Override
    @SneakyThrows
    public long getSize() {
        @NotNull final String sql = String.format("SELECT COUNT(1) FROM %s;", getTableName());
        try (@NotNull final Statement statement = connection.createStatement();
             @NotNull final ResultSet resultSet = statement.executeQuery(sql)) {
            resultSet.next();
            return resultSet.getLong("count");
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M remove(@NotNull final M model) {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE %s = ?;", getTableName(), DmlConstant.ROW_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final Collection<M> collection) {
        @NotNull final String sql = String.format("DELETE FROM %s;", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        remove(model);
        return model;
    }

}
