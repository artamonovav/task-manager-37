package ru.t1.artamonov.tm.api.repository;

import ru.t1.artamonov.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

}
