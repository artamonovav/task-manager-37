package ru.t1.artamonov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void clear();

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator);

    @NotNull
    M add(@NotNull M model);

    @NotNull
    M update(@NotNull M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    boolean existsById(@NotNull String id);

    @Nullable
    M findOneById(@NotNull String id);

    long getSize();

    @Nullable
    M remove(@NotNull M model);

    void removeAll(@Nullable Collection<M> collection);

    @Nullable
    M removeById(@NotNull String id);

}
