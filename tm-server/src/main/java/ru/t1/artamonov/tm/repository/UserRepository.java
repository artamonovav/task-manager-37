package ru.t1.artamonov.tm.repository;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.constant.DmlConstant;
import ru.t1.artamonov.tm.api.repository.IUserRepository;
import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Getter
    @NotNull
    private final String tableName = DmlConstant.USER_TABLE;

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    @SneakyThrows
    protected @NotNull User fetch(@NotNull final ResultSet row) {
        @NotNull final User user = new User();
        user.setId(row.getString(DmlConstant.ROW_ID));
        user.setLogin(row.getString(DmlConstant.LOGIN));
        user.setPasswordHash(row.getString(DmlConstant.PSWRD_HASH));
        user.setFirstName(row.getString(DmlConstant.FST_NAME));
        user.setLastName(row.getString(DmlConstant.LST_NAME));
        user.setMiddleName(row.getString(DmlConstant.MDL_NAME));
        user.setEmail(row.getString(DmlConstant.EMAIL));
        user.setRole(Role.toRole(row.getString(DmlConstant.ROLE)));
        user.setLocked(row.getBoolean(DmlConstant.LOCK_FLG));
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User add(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);",
                getTableName(),
                DmlConstant.ROW_ID, DmlConstant.LOGIN, DmlConstant.PSWRD_HASH,
                DmlConstant.FST_NAME, DmlConstant.LST_NAME, DmlConstant.MDL_NAME,
                DmlConstant.EMAIL, DmlConstant.ROLE, DmlConstant.LOCK_FLG
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getFirstName());
            statement.setString(5, user.getLastName());
            statement.setString(6, user.getMiddleName());
            statement.setString(7, user.getEmail());
            statement.setString(8, user.getRole().toString());
            statement.setBoolean(9, user.getLocked());
            statement.executeUpdate();
        }
        return user;
    }

    @Override
    @SneakyThrows
    public User update(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?;",
                getTableName(), DmlConstant.LOGIN, DmlConstant.PSWRD_HASH,
                DmlConstant.FST_NAME, DmlConstant.LST_NAME, DmlConstant.MDL_NAME,
                DmlConstant.EMAIL, DmlConstant.ROLE, DmlConstant.LOCK_FLG,
                DmlConstant.ROW_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPasswordHash());
            statement.setString(3, user.getFirstName());
            statement.setString(4, user.getLastName());
            statement.setString(5, user.getMiddleName());
            statement.setString(6, user.getEmail());
            statement.setString(7, user.getRole().toString());
            statement.setBoolean(8, user.getLocked());
            statement.setString(9, user.getId());
            statement.executeUpdate();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? LIMIT 1;",
                getTableName(), DmlConstant.LOGIN
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? LIMIT 1;",
                getTableName(), DmlConstant.EMAIL
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

}
