package ru.t1.artamonov.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.artamonov.tm.api.service.IPropertyService;
import ru.t1.artamonov.tm.model.User;
import ru.t1.artamonov.tm.service.PropertyService;
import ru.t1.artamonov.tm.util.HashUtil;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public final class UserTestData {

    @NotNull
    public final static User USER1 = new User();

    @NotNull
    public final static User USER2 = new User();

    @NotNull
    public final static User ADMIN = new User();

    @NotNull
    public final static List<User> USER_LIST1 = Arrays.asList(USER1, USER2);

    @NotNull
    public final static List<User> USER_LIST2 = Arrays.asList(USER2, ADMIN);

    @NotNull
    public final static String UNIT_TEST_USER_LOGIN = UUID.randomUUID().toString();

    @NotNull
    public final static String UNIT_TEST_USER_PASSWORD = "UNIT_TEST_PASSWORD";

    @NotNull
    public final static String UNIT_TEST_USER_EMAIL = "unit_test@email.test";

    @NotNull
    public final static String UNIT_TEST_INCORRECT_LOGIN = "INCORRECT_LOGIN";

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    static {
        USER1.setLogin(UUID.randomUUID().toString());
        USER1.setPasswordHash(HashUtil.salt(propertyService, UNIT_TEST_USER_PASSWORD));
        USER1.setEmail(UNIT_TEST_USER_EMAIL);
        USER2.setLogin(UUID.randomUUID().toString());
        USER2.setPasswordHash(HashUtil.salt(propertyService, UNIT_TEST_USER_PASSWORD));
        USER2.setEmail(UNIT_TEST_USER_EMAIL);
        ADMIN.setLogin(UUID.randomUUID().toString());
        ADMIN.setPasswordHash(HashUtil.salt(propertyService, UNIT_TEST_USER_PASSWORD));
        ADMIN.setEmail(UNIT_TEST_USER_EMAIL);
    }

}
