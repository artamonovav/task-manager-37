-- Table: public.tm_project

-- DROP TABLE IF EXISTS public.tm_project;

CREATE TABLE IF NOT EXISTS public.tm_project
(
    row_id character varying(50) COLLATE pg_catalog."default" NOT NULL,
    created timestamp without time zone,
    name character varying(100) COLLATE pg_catalog."default",
    descrptn character varying(2000) COLLATE pg_catalog."default",
    status character varying(50) COLLATE pg_catalog."default",
    user_id character varying(50) COLLATE pg_catalog."default",
    CONSTRAINT tm_project_pkey PRIMARY KEY (row_id),
    CONSTRAINT user_id_fk FOREIGN KEY (user_id)
        REFERENCES public.tm_user (row_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_project
    OWNER to tm;

COMMENT ON TABLE public.tm_project
    IS 'TASK MANAGER PROJECTS TABLE';