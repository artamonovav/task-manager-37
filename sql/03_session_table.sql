-- Table: public.tm_session

-- DROP TABLE IF EXISTS public.tm_session;

CREATE TABLE IF NOT EXISTS public.tm_session
(
    row_id character varying(50) COLLATE pg_catalog."default" NOT NULL,
    created timestamp without time zone,
    user_id character varying(50) COLLATE pg_catalog."default",
    role character varying(100) COLLATE pg_catalog."default",
    CONSTRAINT tm_session_pkey PRIMARY KEY (row_id),
    CONSTRAINT user_id_fk FOREIGN KEY (user_id)
        REFERENCES public.tm_user (row_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_session
    OWNER to tm;

COMMENT ON TABLE public.tm_session
    IS 'TASK MANAGER SESSION TABLE';
-- Index: fki_U

-- DROP INDEX IF EXISTS public."fki_U";

CREATE INDEX IF NOT EXISTS "fki_U"
    ON public.tm_session USING btree
    (user_id COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;